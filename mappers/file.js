'use strict'


exports.toModel = entity => {
    var model = {
        _id: entity._id
    }
    if (entity.images) {
        model.images = [{
            url: entity.images.url
        }]
    }
    return model
}