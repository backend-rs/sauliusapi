'use strict'

exports.toModel = entity => {
    var model = {
        _id: entity._id,
        name: entity.name,
        status: entity.status
    }

    return model
}