'use strict'

// Products Mapper
exports.toModel = entity => {
    var model = {
        _id: entity._id,
        name: entity.name,
        brand: entity.brand,
        status: entity.status,
        isAdded: entity.isAdded
    }

    if (entity.heading) {
        model.heading = {
            title: entity.heading.title,
            description: entity.heading.description
        }
    }

    if (entity.image) {
        model.image = {
            url: entity.image.url
        }
    }
    if (entity.dimensions) {
        model.dimensions = [{
            value: entity.dimensions[0]['value'],
            unit: entity.dimensions[0]['unit']
        }]
    }
    if (entity.price) {
        model.price = {
            value: entity.price.value,
            currency: entity.price.currency
        }
    }

    return model
}














































// exports.toModel = entity => {
//     var model = {
//         // _id: entity._id,
//         // productName: entity.productType.myProduct.productName
//     }
//     if (entity.myProduct) {
//         model.myProduct = [{
//             _id: entity._id,
//             productName: entity.myProduct[0]['productName'],
//             brand:entity.myProduct[0]['brand']
//         }]
//     }
//     if(entity.similarProduct){
//         model.similarProduct={
//             categoryName: entity.similarProduct.categoryName,
//             products:[{
//             _id: entity._id,
//             productName: entity.similarProduct.products[0]['productName'],
//             brand: entity.similarProduct.products[0]['brand']

//             }]

//         }
//     }
//     return model
// }