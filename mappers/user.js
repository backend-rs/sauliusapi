'use strict'

// users create mapper
exports.toModel = entity => {

    var model = {
        _id: entity._id,
        type: entity.type,
        isVerified: entity.isVerified,
        name: entity.name,
        email: entity.email,
        phone: entity.phone,
        otp:entity.otp,
        expiryTime: entity.expiryTime,
        regToken: entity.regToken
    }
    if (entity.address) {
        model.address = {
            line1: entity.address.line1,
            line2: entity.address.line2,
            city: entity.address.city,
            district: entity.address.district,
            state: entity.address.state,
            pinCode: entity.address.pinCode,
            country: entity.address.country,
            location: {
                longitude: entity.address.location.longitude,
                latitude: entity.address.location.latitude
            }
        }
    }

    return model
}

// for verifyRegOtp
exports.toVerifyOtp = entity => {
    var model = {
        _id: entity._id,
        isVerified: entity.isVerified,
        type: entity.type,
        name: entity.name,
        email: entity.email,
    }
    return model
}

// for login 
exports.toUser = entity => {
    var model = {
        _id: entity._id,
        type: entity.type,
        isVerified: entity.isVerified,
        name: entity.name,
        email: entity.email,
        token: entity.token,
        phone: entity.phone,
    }
    return model
}

// for particular user
exports.toGetUser = entity => {
    var model = {
        _id: entity._id,
        type: entity.type,
        isVerified: entity.isVerified,
        name: entity.name,
        email: entity.email,
        phone: entity.phone,
        chapters:entity.chapters
    }
    if (entity.address) {
        model.address = {
            line1: entity.address.line1,
            line2: entity.address.line2,
            city: entity.address.city,
            district: entity.address.district,
            state: entity.address.state,
            pinCode: entity.address.pinCode,
            country: entity.address.country,
            location: {
                longitude: entity.address.location.longitude,
                latitude: entity.address.location.latitude
            }
        }
    }
    return model
}

// for send token
exports.toSendToken = entity => {
    return {
        tempToken: entity.tempToken
    }
}