'use strict'
const service = require('./similarProducts')
const imageUploadService = require('../services/files')

//  model pass in response 
const createTempProductObj = async (products, count, skipCount, totalCount) => {
    var productObj = {
        categories: products,
        count: count,
        skipCount: skipCount,
        totalCount: totalCount
    }

    return productObj;
}

// set method to update category
const set = async (body, req, entity, context) => {
    try {
        let data
        if (req) {
            if (req.files != null && req.files != undefined) {
                if (req.files.file != null && req.files.file != undefined) {

                    if (body.shouldImageUpdate == "true") {

                        data = await imageUploadService.upload(req.files.file)

                        if (body.name) {
                            entity.name = body.name
                        }
                        if (data.url) {
                            entity.image.url = data.url
                        }
                        if (data.thumbnail) {
                            entity.image.thumbnail = data.thumbnail
                        }
                        if (data.resize_url) {
                            entity.image.resize_url = data.resize_url
                        }
                        if (data.resize_thumbnail) {
                            entity.image.resize_thumbnail = data.resize_thumbnail
                        }
                        if (body.status) {
                            entity.status = body.status
                        }
                        if (body.shouldImageUpdate) {
                            entity.shouldImageUpdate = body.shouldImageUpdate
                        }
                    } else {
                        if (body.name) {
                            entity.name = body.name
                        }
                        if (body.status) {
                            entity.status = body.status
                        }
                    }
                }
            }
        }

        return entity

    } catch (err) {
        throw new Error(err)
    }
}


const create = async (req, body, context) => {
    const log = context.logger.start(`services/categories`)

    try {
        let category
        if (body.isUpdate == "true") {
            // if isUpdate = true then update category

            category = await update(req, body.category_id, body, context)
        } else {
            // if !isUpdate then create product

            // find category
            category = await db.category.findOne({
                'name': {
                    $regex: new RegExp("^" + body.name + "$", "i")
                }
            })

            // if  exist
            if (category) {
                throw new Error('Category already exist')
            }

            // if category doesn't exist
            let data = await imageUploadService.upload(req.files.file)

            let categoryTemp = {}
            const temp = {
                image: {
                    url: data.url,
                    thumbnail: data.thumbnail,
                    resize_url: data.resize_url,
                    resize_thumbnail: data.resize_thumbnail
                }
            }

            categoryTemp = body
            categoryTemp.image = temp.image

            category = await new db.category(categoryTemp).save()
        }
        log.end()
        return category

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    try {
        var req = {
            query: {
                '_id': id
            }
        }
        const category = await db.category.findById(id)
        const similarProducts = await service.getByCategory(req)
        category.products = similarProducts.products;

        category.count = ''
        category.skipCount = ''
        category.totalCount = ''
        return category
    } catch (err) {
        throw new Error(err)
    }
}

const get = async (req) => {
    try {
        const params = req.query;
        let category;
        let similarProducts;

        if (params && (params._id != undefined && params._id != null) && (params.status != undefined && params.status != null) && (params.pageNo == undefined && params.pageNo == null) && (params.items == undefined && params.items == null)) {

            // find category
            category = await db.category.findOne({
                '_id': {
                    $eq: params._id
                }
            })
            if (!category) {
                throw new Error('Category not found')
            }

            // find similarProduct
            similarProducts = await service.getByCategory(req)

            category.products = similarProducts.products;
            category.count = ''
            category.skipCount = ''
            category.totalCount = ''


        } else if (params && (params._id != undefined && params._id != null) && (params.status == undefined && params.status == null) && (params.pageNo == undefined && params.pageNo == null) && (params.items == undefined && params.items == null)) {

            // find category
            category = await db.category.findOne({
                '_id': {
                    $eq: params._id
                }
            })
            if (!category) {
                throw new Error('Category not found')
            }

            // find similarProduct
            similarProducts = await service.getByCategory(req)

            category.products = similarProducts.products;
            category.count = ''
            category.skipCount = ''
            category.totalCount = ''

        } else if (params && (params._id != undefined && params._id != null) && (params.status != undefined && params.status != null) && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {

            // find category
            category = await db.category.findOne({
                '_id': {
                    $eq: params._id
                }
            })
            if (!category) {
                throw new Error('Category not found')
            }

            // find similarProduct
            similarProducts = await service.getByCategory(req)

            category.products = similarProducts.products;
            category.count = similarProducts.count
            category.skipCount = similarProducts.skipCount
            category.totalCount = similarProducts.totalCount


        } else if (params && (params._id != undefined && params._id != null) && (params.status == undefined && params.status == null) && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {

            //find category
            category = await db.category.findOne({
                '_id': {
                    $eq: params._id
                }
            })
            if (!category) {
                throw new Error('Category not found')
            }

            // find similarProduct
            similarProducts = await service.getByCategory(req)

            category.products = similarProducts.products;
            category.count = similarProducts.count
            category.skipCount = similarProducts.skipCount
            category.totalCount = similarProducts.totalCount


        } else if (params && (params.status != undefined && params.status != null) && (params.pageNo == undefined && params.pageNo == null) && (params.items == undefined && params.items == null)) {

            // find category
            category = await db.category.find({
                'status': {
                    $eq: params.status
                }
            }).sort({
                timeStamp: -1
            })

            let totallllll = await db.category.find({
                'status': {
                    $eq: params.status
                }
            }).count()

            let count = ''
            let skippedCount = ''
            let totalCount = totallllll

            const tempProductResponseObj = await createTempProductObj(category, count, skippedCount, totalCount)
            category = tempProductResponseObj

        } else if (params && (params.status != undefined && params.status != null) && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {

            let pageNo = Number(params.pageNo) || 1
            let items = Number(params.items) || 10
            let skipCount = items * (pageNo - 1)

            // find category
            category = await db.category.find({
                'status': {
                    $eq: params.status
                }
            }).skip(skipCount).limit(items).sort({
                timeStamp: -1
            })

            // total category
            let totalCount = await db.category.find({
                'status': {
                    $eq: params.status
                }
            }).count()

            // total skipped
            let skippedCount = skipCount

            // requested items
            let count = items

            const tempProductResponseObj = await createTempProductObj(category, count, skippedCount, totalCount)
            category = tempProductResponseObj

        } else if (params && (params.status == undefined && params.status == null) && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {

            let pageNo = Number(params.pageNo) || 1
            let items = Number(params.items) || 10
            let skipCount = items * (pageNo - 1)

            // find category
            category = await db.category.find({}).skip(skipCount).limit(items).sort({
                timeStamp: -1
            })

            // total category
            let totalCount = await db.category.find({}).count()

            // total skipped
            let skippedCount = skipCount

            // requested items
            let count = items

            const tempProductResponseObj = await createTempProductObj(category, count, skippedCount, totalCount)
            category = tempProductResponseObj

        } else {

            category = await db.category.find({}).sort({
                timeStamp: -1
            })

            let count = ''
            let skippedCount = ''
            let totalCount = ''

            const tempProductResponseObj = await createTempProductObj(category, count, skippedCount, totalCount)
            category = tempProductResponseObj
        }
        return category

    } catch (err) {
        throw new Error(err)
    }
}

const update = async (req, id, body, context) => {
    try {
        let category

        const entity = await db.category.findById(id)
        if (!entity) {
            throw new Error('invalid product')
        }

        if ((body.name != undefined && body.name != null)) {

            // find category
            category = await db.category.findOne({
                'name': {
                    $regex: new RegExp("^" + body.name + "$", "i")
                }
            })

            // if exist
            if (category) {
                if (category.id != entity.id) {
                    throw new Error('Category already exist')
                }
            }
        }

        var request = {
            query: {
                '_id': id
            }
        }
        const similarProducts = await service.getByCategory(request)
        let data = []
        if (similarProducts) {
            for (const product of similarProducts.products) {
                data.push({
                    _id: product.id
                })
            }
            entity.products = data
        }

        // if product doesn't exist
        category = await set(body, req, entity, context)

        return category.save()
    } catch (err) {
        throw new Error(err)
    }
}

exports.create = create
exports.getById = getById
exports.get = get
exports.update = update