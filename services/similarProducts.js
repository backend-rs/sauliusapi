'use strict'
const imageUploadService = require('../services/files')

// model pass in response
const createTempProductObj = async (product, count, skipCount, totalCount) => {
    var productObj = {
        products: product,
        count: count,
        skipCount: skipCount,
        totalCount: totalCount
    }

    return productObj;
}

// set method to update similarProduct
const set = async (body, req, entity, context) => {
    try {
        let data
        if (req) {
            if (req.files != null && req.files != undefined) {
                if (req.files.file != null && req.files.file != undefined) {

                    if (body.shouldImageUpdate == "true") {

                        data = await imageUploadService.upload(req.files.file)

                        if (body.name) {
                            entity.name = body.name
                        }
                        if (body.brand) {
                            entity.brand = body.brand
                        }
                        if (data.url) {
                            entity.image.url = data.url
                        }
                        if (data.thumbnail) {
                            entity.image.thumbnail = data.thumbnail
                        }
                        if (data.resize_url) {
                            entity.image.resize_url = data.resize_url
                        }
                        if (data.resize_thumbnail) {
                            entity.image.resize_thumbnail = data.resize_thumbnail
                        }
                        if (body.heading && body.heading.title) {
                            entity.heading.title = body.heading.title
                        }
                        if (body.heading && body.heading.description) {
                            entity.heading.description = body.heading.description
                        }
                        if (!entity.dimensions[0]) {
                            if (body.dimensions && body.dimensions.value) {
                                entity.dimensions[0] = body.dimensions
                            }
                        } else {
                            if (body.dimensions && body.dimensions.value) {
                                entity.dimensions[0].value = body.dimensions.value
                            }
                            if (body.dimensions && body.dimensions.unit) {
                                entity.dimensions[0].unit = body.dimensions.unit
                            }
                        }
                        if (body.price && body.price.value) {
                            entity.price.value = body.price.value
                        }
                        if (body.price && body.price.currency) {
                            entity.price.currency = body.price.currency
                        }
                        if (body.status) {
                            entity.status = body.status
                        }
                        if (body.isAdded) {
                            entity.isAdded = body.isAdded
                        }
                        if (body.shouldImageUpdate) {
                            entity.shouldImageUpdate = body.shouldImageUpdate
                        }

                    } else {

                        if (body.name) {
                            entity.name = body.name
                        }
                        if (body.brand) {
                            entity.brand = body.brand
                        }
                        if (body.heading && body.heading.title) {
                            entity.heading.title = body.heading.title
                        }
                        if (body.heading && body.heading.description) {
                            entity.heading.description = body.heading.description
                        }
                        if (!entity.dimensions[0]) {
                            if (body.dimensions && body.dimensions.value) {
                                entity.dimensions[0] = body.dimensions
                            }
                        } else {
                            if (body.dimensions && body.dimensions.value) {
                                entity.dimensions[0].value = body.dimensions.value
                            }
                            if (body.dimensions && body.dimensions.unit) {
                                entity.dimensions[0].unit = body.dimensions.unit
                            }
                        }
                        if (body.price && body.price.value) {
                            entity.price.value = body.price.value
                        }
                        if (body.price && body.price.currency) {
                            entity.price.currency = body.price.currency
                        }
                        if (body.status) {
                            entity.status = body.status
                        }
                        if (body.isAdded) {
                            entity.isAdded = body.isAdded
                        }

                    }
                }
            }
        }

        return entity
    } catch (err) {
        throw new Error(err)
    }
}


const create = async (req, body, context) => {
    try {
        let similarProducts

        if (body.isUpdate == "true") {
            // if isUpdate = true then update product

            similarProducts = await update(req, body.product_id, body, context)
        } else {
            // if !isUpdate then create product


            // find similarProduct
            similarProducts = await db.similarProduct.findOne({
                'name': {
                    $regex: new RegExp("^" + body.name + "$", "i")
                },
                'dimensions': {
                    $elemMatch: {
                        'value': {
                            $regex: new RegExp("^" + body.dimensions.value + "$", "i")
                        },
                        'unit': {
                            $regex: new RegExp("^" + body.dimensions.unit + "$", "i")
                        }
                    }
                }
            })

            // if  exist
            if (similarProducts) {
                throw new Error('Product already exist')
            }

            // if similarProduct doesn't exist
            let productTemp = {}
            let data = await imageUploadService.upload(req.files.file)

            const temp = {
                image: {
                    url: data.url,
                    thumbnail: data.thumbnail,
                    resize_url: data.resize_url,
                    resize_thumbnail: data.resize_thumbnail
                }

            }

            productTemp = body
            productTemp.image = temp.image

            similarProducts = await new db.similarProduct(productTemp).save()
        }

        return similarProducts
    } catch (err) {
        throw new Error(err)
    }
}

const getByCategory = async (req) => {
    try {

        const params = req.query;
        let similarProducts;

        if (params && (params._id != undefined && params._id != null) && (params.status == undefined && params.status == null) && (params.pageNo == undefined && params.pageNo == null) && (params.items == undefined && params.items == null)) {

            // find products
            similarProducts = await db.similarProduct.find({
                'category._id': {
                    $eq: params._id
                }
            }).sort({
                timeStamp: -1
            })

            let count = ''
            let skippedCount = ''
            let totalCount = ''

            const tempProductResponseObj = await createTempProductObj(similarProducts, count, skippedCount, totalCount)
            similarProducts = tempProductResponseObj

        } else if (params && (params._id != undefined && params._id != null) && (params.status != undefined && params.status != null) && (params.pageNo == undefined && params.pageNo == null) && (params.items == undefined && params.items == null)) {

            // find products
            similarProducts = await db.similarProduct.find({
                'category._id': {
                    $eq: params._id
                },
                'status': {
                    $eq: params.status
                }
            }).sort({
                timeStamp: -1
            })

            let count = ''
            let skippedCount = ''
            let totalCount = ''

            const tempProductResponseObj = await createTempProductObj(similarProducts, count, skippedCount, totalCount)
            similarProducts = tempProductResponseObj

        } else if (params && (params._id != undefined && params._id != null) && (params.status != undefined && params.status != null) && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {


            let pageNo = Number(params.pageNo) || 1
            let items = Number(params.items) || 10
            let skipCount = items * (pageNo - 1)

            // find products
            similarProducts = await db.similarProduct.find({
                'category._id': {
                    $eq: params._id
                },
                'status': {
                    $eq: params.status
                }
            }).skip(skipCount).limit(items).sort({
                timeStamp: -1
            })

            // total products
            let totalCount = await db.similarProduct.find({
                'category._id': {
                    $eq: params._id
                },
                'status': {
                    $eq: params.status
                }
            }).count()

            // total skipped
            let skippedCount = skipCount

            // requested items
            let count = items

            const tempProductResponseObj = await createTempProductObj(similarProducts, count, skippedCount, totalCount)
            similarProducts = tempProductResponseObj


        } else if (params && (params._id != undefined && params._id != null) && (params.status == undefined && params.status == null) && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {


            let pageNo = Number(params.pageNo) || 1
            let items = Number(params.items) || 10
            let skipCount = items * (pageNo - 1)

            // find products
            similarProducts = await db.similarProduct.find({
                'category._id': {
                    $eq: params._id
                }
            }).skip(skipCount).limit(items).sort({
                timeStamp: -1
            })

            // total products
            let totalCount = await db.similarProduct.find({
                'category._id': {
                    $eq: params._id
                }
            }).count()

            // total skipped
            let skippedCount = skipCount

            // requested items
            let count = items

            const tempProductResponseObj = await createTempProductObj(similarProducts, count, skippedCount, totalCount)
            similarProducts = tempProductResponseObj


        } else {

            // find products
            similarProducts = await db.similarProduct.find({}).sort({
                timeStamp: -1
            })

            let count = ''
            let skippedCount = ''
            let totalCount = ''

            const tempProductResponseObj = await createTempProductObj(similarProducts, count, skippedCount, totalCount)
            similarProducts = tempProductResponseObj
        }


        return similarProducts

    } catch (err) {
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    try {
        const similarProducts = await db.similarProduct.findById(id)
        return similarProducts
    } catch (err) {
        throw new Error(err)
    }
}

const update = async (req, id, body, context) => {
    try {
        let similarProduct

        const entity = await db.similarProduct.findById(id)
        if (!entity) {
            throw new Error('invalid similarProduct')
        }

        if ((body.name != undefined && body.name != null) && (body.dimensions.value != undefined && body.dimensions.value != null) && (body.dimensions.unit != undefined && body.dimensions.unit != null)) {

            // find similarProduct
            similarProduct = await db.similarProduct.findOne({
                'name': {
                    $regex: new RegExp("^" + body.name + "$", "i")
                },
                'dimensions': {
                    $elemMatch: {
                        'value': {
                            $regex: new RegExp("^" + body.dimensions.value + "$", "i")
                        },
                        'unit': {
                            $regex: new RegExp("^" + body.dimensions.unit + "$", "i")
                        }
                    }
                }
            })

            // if  exist
            if (similarProduct) {
                if (similarProduct.id != entity.id) {
                    throw new Error('Product already exist')
                }

            }
        }

        // if similarProduct doesn't exist
        similarProduct = await set(body, req, entity, context)

        return similarProduct.save()
    } catch (err) {
        throw new Error(err)
    }
}

exports.create = create
exports.getByCategory = getByCategory
exports.getById = getById
exports.update = update