// 'use strict'

const fs = require('fs')
const Jimp = require('jimp')
const path = require('path')
const appRoot = require('app-root-path')
const fileStoreConfig = require('config').get('providers.file-store')


// function to upload image
const upload = async (file, context) => {

    let parts = file.name.split('.')

    let name = parts[0]
    let ext = parts[1]

    let destDir = path.join(appRoot.path, fileStoreConfig.dir)

    let fileName = `${name}-${Date.now()}.${ext}`

    let destination = `${destDir}/${fileName}`
    let url = `${fileStoreConfig.root}/${fileName}`

    await move(file.path, destination)

    const thumbnail = await imagethumbnail(destination)

    file.path = destination
    const resize = await resizeImage(file, context)

    return {
        url: url,
        thumbnail: thumbnail,
        resize_url: resize.url,
        resize_thumbnail: resize.thumbnail
    }
}

const move = async (oldPath, newPath) => {
    const copy = (cb) => {
        var readStream = fs.createReadStream(oldPath)
        var writeStream = fs.createWriteStream(newPath)

        readStream.on('error', cb)
        writeStream.on('error', cb)

        readStream.on('close', function () {
            fs.unlink(oldPath, cb)
        })

        readStream.pipe(writeStream)
    }

    return new Promise((resolve, reject) => {
        fs.rename(oldPath, newPath, function (err) {
            if (err) {
                if (err.code === 'EXDEV') {
                    copy(err => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve()
                        }
                    })
                } else {
                    reject(err)
                }
            } else {
                resolve()
            }
        })
    })
}

const resizeImage = async (file, context) => {
    let parts = file.name.split('.')

    let name = parts[0]
    let ext = parts[1]

    let destDir = path.join(appRoot.path, fileStoreConfig.dir)

    let fileName = `${name}-${Date.now()}.${ext}`

    let destination = `${destDir}/${fileName}`
    let url = `${fileStoreConfig.root}/${fileName}`

    await Jimp.read(file.path)
        .then(lenna => {
            return lenna
                .resize(256, 256) // resize
                .quality(60) // set JPEG quality
                .write(path.join(destination)) // save
        })
        .catch(err => {
            throw new Error(err)
        })

    const thumbnail = await imagethumbnail(destination)

    return {
        url,
        destination,
        thumbnail
    }
}

const imagethumbnail = (path) => {
    if (!path) {
        return Promise.resolve(null)
    }

    return new Promise((resolve, reject) => {
        return Jimp.read(path).then(function (lenna) {
            if (!lenna) {
                return resolve(null)
            }
            var a = lenna.resize(15, 15) // resize
                .quality(50) // set JPEG quality
                .getBase64(Jimp.MIME_JPEG, function (result, base64, src) {
                    return resolve(base64).save()
                })
        }).catch(function (err) {
            reject(err)
        })
    })
}



exports.upload = upload






























// exports.create = create
// exports.getById = getById
// exports.get = get
// exports.update = update


















// const set = async (body, req, entity, context) => {
    //     try {
    
    //         let data
    //         if (req) {
    //             if (req.files != null && req.files != undefined) {
    //                 if (req.files.file != null && req.files.file != undefined) {
    //                     if (body.shouldImageUpdate == "true") {
    
    //                         data = await sliderUpload(req.files.file, )
    
    //                         if (data) {
    //                             if (entity.images.length <= 3) {
    //                                 entity.images.push({
    //                                     url: data.url,
    //                                     thumbnail: data.thumbnail,
    //                                     resize_url: data.resize_url,
    //                                     resize_thumbnail: data.resize_thumbnail
    //                                 })
    //                             } else {
    //                                 entity.images.shift()
    //                                 entity.images.push({
    //                                     url: data.url,
    //                                     thumbnail: data.thumbnail,
    //                                     resize_url: data.resize_url,
    //                                     resize_thumbnail: data.resize_thumbnail
    //                                 })
    //                             }
    //                         }
    
    //                     }
    //                 }
    //             }
    //         }
    //         return entity
    //     } catch (err) {
    //         throw new Error(err)
    //     }
    // }
    // const create = async (req, body, context) => {
    //     try {
    //         let file
    //         if (body.isUpdate == "true") {
    //             file = await update(req, body.file_id, body, context)
    //         } else {
    //             file = await new db.file(body).save()
    //         }
    //         return file
    
    //     } catch (err) {
    //         throw new Error(err)
    //     }
    // }
    
    // const getById = async (id, context) => {
    //     try {
    //         const file = await db.file.findById(id)
    //         return file
    //     } catch (err) {
    //         throw new Error(err)
    //     }
    // }
    // const get = async (req, context) => {
    //     try {
    //         let params = req.query
    //         let file
    //         let temp = {}
    //         if (params && (params.pageNo != undefined && params.pageNo != null) && (params.items != undefined && params.items != null)) {
    
    //             let index = JSON.parse(params.items)
    
    //             let pageNo = Number(params.pageNo) || 1
    //             let items = Number(params.items) || 10
    //             let skipCount = items * (pageNo - 1)
    
    //             file = await db.file.find({})
    //             console.log(file)
    //             for (const item of file) {
    //                 if (item) {
    //                     temp = item.images[index - 1]
    //                 }
    //             }
    //             file = temp
    //         } else {
    //             file = await db.file.find({})
    //         }
    
    //         return file
    //     } catch (err) {
    //         throw new Error(err)
    //     }
    // }
    
    
    // const update = async (req, id, body, context) => {
    //     try {
    //         let i = 0000
    //         let file
    //         const entity = await db.file.findById(id)
    //         if (!entity) {
    //             throw new Error('invalid product')
    //         }
    
    //         if (req != null && req != undefined) {
    //             file = await set(body, req, entity, context)
    //         } else {
    
    //             file = await db.file.findById(id)
    
    //             if (file) {
    //                 if (file.totalBills == undefined && file.totalBills == null) {
    //                     file.totalBills = 0;
    //                     file.totalBills = file.totalBills + 1;
    //                 } else {
    //                     file.totalBills += 1
    //                 }
    //             }
    //         }
    
    //         return file.save()
    //     } catch (err) {
    //         throw new Error(err)
    //     }
    // }
    // function to upload silder image
// const sliderUpload = async (file, context) => {

//     let parts = file.name.split('.')

//     let name = parts[0]
//     let ext = parts[1]

//     let destDir = path.join(appRoot.path, fileStoreConfig.dir)

//     let fileName = `${name}-${Date.now()}.${ext}`

//     let destination = `${destDir}/${fileName}`
//     let url = `${fileStoreConfig.root}/${fileName}`

//     await sliderMove(file.path, destination)

//     const thumbnail = await sliderImagethumbnail(destination)

//     file.path = destination
//     const resize = await sliderResizeImage(file, context)

//     return {
//         url: url,
//         thumbnail: thumbnail,
//         resize_url: resize.url,
//         resize_thumbnail: resize.thumbnail
//     }
// }

// const sliderMove = async (oldPath, newPath) => {
//     const copy = (cb) => {
//         var readStream = fs.createReadStream(oldPath)
//         var writeStream = fs.createWriteStream(newPath)

//         readStream.on('error', cb)
//         writeStream.on('error', cb)

//         readStream.on('close', function () {
//             fs.unlink(oldPath, cb)
//         })

//         readStream.pipe(writeStream)
//     }

//     return new Promise((resolve, reject) => {
//         fs.rename(oldPath, newPath, function (err) {
//             if (err) {
//                 if (err.code === 'EXDEV') {
//                     copy(err => {
//                         if (err) {
//                             reject(err)
//                         } else {
//                             resolve()
//                         }
//                     })
//                 } else {
//                     reject(err)
//                 }
//             } else {
//                 resolve()
//             }
//         })
//     })
// }

// const sliderResizeImage = async (file, context) => {
//     let parts = file.name.split('.')

//     let name = parts[0]
//     let ext = parts[1]

//     let destDir = path.join(appRoot.path, fileStoreConfig.dir)

//     let fileName = `${name}-${Date.now()}.${ext}`

//     let destination = `${destDir}/${fileName}`
//     let url = `${fileStoreConfig.root}/${fileName}`

//     await Jimp.read(file.path)
//         .then(lenna => {
//             return lenna
//                 .resize(512, 288) // resize
//                 .quality(100) // set JPEG quality
//                 .write(path.join(destination)) // save
//         })
//         .catch(err => {
//             throw new Error(err)
//         })

//     const thumbnail = await sliderImagethumbnail(destination)

//     return {
//         url,
//         destination,
//         thumbnail
//     }
// }

// const sliderImagethumbnail = (path) => {
//     if (!path) {
//         return Promise.resolve(null)
//     }

//     return new Promise((resolve, reject) => {
//         return Jimp.read(path).then(function (lenna) {
//             if (!lenna) {
//                 return resolve(null)
//             }
//             var a = lenna.resize(15, 15) // resize
//                 .quality(50) // set JPEG quality
//                 .getBase64(Jimp.MIME_JPEG, function (result, base64, src) {
//                     return resolve(base64).save()
//                 })
//         }).catch(function (err) {
//             reject(err)
//         })
//     })
// }
    