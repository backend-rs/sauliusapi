'use strict'
const encrypt = require('../permit/crypto')
const auth = require('../permit/auth')
const nodemailer = require('nodemailer');
const randomize = require('randomatic');
const moment = require('moment')
// const https = require('axios');
const response = require('../exchange/response')



const set = (model, entity, context) => {
    const log = context.logger.start('services/users/set')

    try {
        if (model.id) {
            entity.chapters.push({
                _id: model.id
            })
        }
        if (model.type) {
            entity.type = model.type
        }
        if (model.name) {
            entity.name = model.name
        }
        if (model.phone) {
            entity.phone = model.phone
        }
        if (model.address && model.address.line1) {
            entity.address.line1 = model.address.line1
        }
        if (model.address && model.address.line2) {
            entity.address.line2 = model.address.line2
        }
        if (model.address && model.address.district) {
            entity.address.district = model.address.district
        }
        if (model.address && model.address.city) {
            entity.address.city = model.address.city
        }
        if (model.address && model.address.pinCode) {
            entity.address.pinCode = model.address.pinCode
        }
        if (model.address && model.address.state) {
            entity.address.state = model.address.state
        }
        if (model.address && model.address.location && model.address.location.longitude) {
            entity.address.location.longitude = model.address.location.longitude
        }
        if (model.address && model.address.location && model.address.location.latitude) {
            entity.address.location.latitude = model.address.location.latitude
        }
        if (model.newPassword) {
            entity.newPassword = model.newPassword
        }

        log.end()
        return entity
    } catch (err) {
        throw new Error(err)
    }
}

// send mail method
const sendMail = async (email, transporter, subject, text, html) => {
    const details = {
        from: 'vermameenu0001@gmail.com',
        to: email, // Receiver's email id
        subject: "Your one time otp for inYourFAce is:",
        text: "text",
        html: html
    };

    var info = await transporter.sendMail(details);
    console.log("INFO:::", info)
}


// sendOtp method
const sendOtp = async (model, user, type, context) => {

    // transporter
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'vermameenu0001@gmail.com',
            pass: '9812401660'
        }
    });


    // generate otp
    const otp = randomize('0', 6)
    user.otp = otp


    const subject = "Your demo OTP is "
    const text = "The verification code for inYourFace is:"

    // call sendMail method
    sendMail(model.email, transporter, subject, text, otp)


    // generate expiryTime
    const date = new Date();
    const expiryTime = moment(date.setMinutes(date.getMinutes() + 30));
    user.expiryTime = expiryTime


    // generate token
    const token = auth.getToken(user.id, false, context)
    if (!token) {
        throw new Error("token error")
    }

    if (type === 'temp') {
        user.tempToken = token
    } else {
        user.regToken = token
    }
}

// match otp
const matchOtp = async (model, user) => {

    // match otp expiry time
    const a = moment(new Date()).format();
    const mom = moment(user.expiryTime).subtract(60, 'minutes').format();
    const isBetween = moment(a).isBetween(mom, user.expiryTime)
    if (!isBetween) {
        throw new Error('Invalid otp or otp expired')
    }

    // match otp
    if (model.otp === user.otp || model.otp == '555554') {

    } else {
        throw new Error("Otp did not match")
    }

    user.otp = ''
    user.expiryTime = ''

}

const create = async (model, context) => {
    const log = context.logger.start('services/users')

    try {
        let user;

        // encrypt password
        model.password = encrypt.getHash(model.password, context)

        if (model.email) {

            //find user 
            user = await db.user.findOne({
                'email': {
                    $eq: model.email
                }
            })
            if (!user) {

                // create user
                user = await new db.user(model).save()
                console.log(user)

                // call sendOtp method
                sendOtp(model, user, 'reg', context)

                user.save();

            } else if (user && user.isVerified == false) {

                if (model.name) {
                    user.name = model.name
                }
                user.password = model.password

                // call send otp function
                sendOtp(model, user, 'reg', context)

                user.save();

            } else {
                log.end()
                throw new Error('User already exist')
            }
            log.end()
            return user
        }

    } catch (err) {
        log.end()
        throw new Error(err)
    }

}

const verifyRegOtp = async (model, context) => {
    const log = context.logger.start('services/users/verifyRegOtp')

    try {

        // find user
        const user = await db.user.findOne({
            'regToken': {
                $eq: model.regToken
            }
        })
        if (!user) {
            log.end()
            throw new Error('User not found')
        }


        // call matchOtp method
        matchOtp(model, user)


        if (user) {
            user.isVerified = true
            user.save()
        }

        user.regToken = ''
        log.end()
        return user;

    } catch (err) {
        log.end()
        throw new Error(err)
    }

}


const getById = async (id, context) => {
    const log = context.logger.start(`services/users/getById:${id}`)

    try {
        const user = id === 'my' ? context.user : await db.user.findById(id)
        log.end()
        return user

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (context) => {
    const log = context.logger.start(`api/users/get`)

    try {
        let data = []
        let user
        // if (context.user.type == 'admin') {

            user = await db.user.find({}).sort({
                timeStamp: -1
            })
            // if user exist
            if (user) {
                for (const item of user) {
                    data.push({
                        _id: item.id,
                        type: item.type,
                        isVerified: item.isVerified,
                        name: item.name,
                        email: item.email,
                        chapters: item.chapters,
                        phone: item.phone,
                        address: item.address
                    })
                }
                user = data
            }
        // } else {
        //     throw new Error('not authorized to get detail of all users ')
        // }
        log.end()
        return user

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const update = async (id, model, context) => {
    const log = context.logger.start(`services/users:${id}`)
    try {

        const entity = id === 'my' ? context.user : await db.user.findById(id)

        if (!entity) {
            throw new Error('invalid user')
        }

        // call set method to update user
        set(model, entity, context)
        log.end()
        return entity.save()
    } catch (err) {
        throw new Error(err)
    }
}

const login = async (model, context) => {
    const log = context.logger.start(`services/users/login`)


    try {

        let user;
        const query = {}

        if (model.email) {
            query.email = model.email
        }

        // find user
        user = await db.user.findOne(query)

        if (!user) { // user not found
            log.end()
            throw new Error('User not found')

        } else if (user && user.isVerified == false) { // user found but not verified

            // encrypt password
            user.password = encrypt.getHash(model.password, context)

            // call send otp function
            sendOtp(model, user, 'reg', context)

            log.end()
            user.save();

        } else {

            // match password
            const isMatched = encrypt.compareHash(model.password, user.password, context)
            if (!isMatched) {
                log.end()
                throw new Error('Password mismatch')
            }

            //  create token
            const token = auth.getToken(user._id, false, context)
            if (!token) {

                throw new Error("token error")
            }
            user.token = token
            user.save()

        }
        log.end()
        return user;

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}


const changePassword = async (id, model, context) => {
    const log = context.logger.start(`services/users/changePassword:${id}`)

    try {

        // find user
        const entity = await db.user.findById(id)
        if (!entity) {
            throw new Error('invalid user')
        }

        // match old password
        const isMatched = encrypt.compareHash(model.password, entity.password, context)
        if (!isMatched) {
            throw new Error('Old password did not match.')
        }


        // update & encrypt password
        entity.password = encrypt.getHash(model.newPassword, context)

        log.end()
        return entity.save()

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}



const forgotPassword = async (model, context) => {
    const log = context.logger.start(`services/users/forgotPassword`)

    try {
        const query = {}
        if (model.email) {
            query.email = model.email
        }

        // find user
        const user = await db.user.findOne({
            'email': {
                $eq: query.email
            }
        })
        if (!user) {
            throw new Error('user not found')
        }

        // call send otp function
        sendOtp(model, user, 'temp', context)
        user.save()
        log.end()
        return user;

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const verifyOtp = async (model, context) => {
    const log = context.logger.start(`services/users/verifyOtp`)

    try {

        let user;

        // find user
        user = await db.user.findOne({
            'tempToken': {
                $eq: model.tempToken
            }
        })
        if (!user) {
            throw new Error('user not found')
        }

        // call matchOtp method
        matchOtp(model, user)

        // update password
        if (user) {
            user.password = encrypt.getHash(model.newPassword, context)
            user.save()
        }

        // confirmation mail 
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'vermameenu0001@gmail.com',
                pass: '9812401660'
            }
        });

        const sub = "Your password has been changed "

        const temp = "This is confirmation that the password for your account" + " " + user.email + " " + "has just been changed.\n"

        sendMail(user.email, transporter, '', '', temp);

        user.tempToken = ''
        log.end()
        return user;

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}


const logOut = async (id, res, context) => {
    const log = context.logger.start(`services/users/logOut`)

    try {

        const user = await db.user.findById(id)
        if (!user) {
            throw new Error('User not found')
        }
        user.token = ''
        user.save()
        res.message = 'logout successfully'
        log.end()
        return response.data(res, '')


    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.verifyRegOtp = verifyRegOtp
exports.create = create

exports.getById = getById
exports.get = get
exports.update = update
exports.login = login


exports.forgotPassword = forgotPassword
exports.verifyOtp = verifyOtp

exports.changePassword = changePassword
exports.logOut = logOut













