'use strict'

const response = require('../exchange/response')
const service = require('../services/similarProducts')


exports.create = async (req, res) => {
    try {
        const similarProducts = await service.create(req,req.body, req.context)
        return response.data(res, similarProducts)
    } catch (err) {
        return response.failure(res, err.message)
    }
}

exports.getByCategory= async (req, res) => {
    try {
        const similarProducts = await service.getByCategory(req)
        return response.data(res, similarProducts)
    } catch (err) {
        return response.failure(res, err.message)
    }
}

exports.getById = async (req, res) => {
    try {
        const similarProducts = await service.getById(req.params.id, req.context)
        return response.data(res, similarProducts)
    } catch (err) {
        return response.failure(res, err.message)
    }
}

exports.update = async (req, res) => {
    try {
        const similarProducts = await service.update(req, req.params.id, req.body, req.context)
        return response.data(res, similarProducts)
    } catch (err) {
        return response.failure(res, err.message)
    }
}