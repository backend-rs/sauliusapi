'use strict'

const response = require('../exchange/response')
const service = require('../services/products')


exports.create = async (req, res) => {
    try {
        const product = await service.create(req, req.body, req.context)
        return response.data(res, product)
    } catch (err) {
        return response.failure(res, err.message)
    }
}

exports.getById = async (req, res) => {
    try {
        const product = await service.getById(req.params.id, req.context)
        return response.data(res, product)
    } catch (err) {
        return response.failure(res, err.message)
    }
}

exports.get = async (req, res) => {
    try {
        const product = await service.get(req)
        return response.data(res, product)
    } catch (err) {
        return response.failure(res, err.message)
    }
}

exports.update = async (req, res) => {
    try {
        const product = await service.update(req, req.params.id, req.body, req.context)
        return response.data(res, product)
    } catch (err) {
        return response.failure(res, err.message)
    }
}

