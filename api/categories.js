'use strict'

const response = require('../exchange/response')
const service = require('../services/categories')


exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/categories`)

    try {
        const category = await service.create(req,req.body, req.context)
        log.end()
        return response.data(res, category)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.getById = async (req, res) => {
    try {
        const category = await service.getById(req.params.id, req.context)
        return response.data(res, category)
    } catch (err) {
        return response.failure(res, err.message)
    }
}

exports.get = async (req, res) => {
    try {
        const category = await service.get(req)
        return response.data(res, category)
    } catch (err) {
        return response.failure(res, err.message)
    }
}

exports.update = async (req, res) => {
    try {
        const category = await service.update(req, req.params.id, req.body, req.context)
        return response.data(res, category)
    } catch (err) {
        return response.failure(res, err.message)
    }
}


