'use strict'

const response = require('../exchange/response')
const service = require('../services/files')
const mapper = require('../mappers/file')

exports.upload = async (req, res) => {
    try {
        let data = await service.upload(req.files.file)
        return response.data(res, data)
    } catch (err) {
        return response.failure(res, err.message)
    }

}






















// exports.create = async (req, res) => {
    //     try {
    //         const file = await service.create(req,req.body, req.context)
    //         return response.data(res, file)
    
    //     } catch (err) {
    //         return response.failure(res, 'images already exists')
    //     }
    // }
    
    // exports.getById = async (req, res) => {
    //     try {
    //         const file = await service.getById(req.params.id, req.context)
    //         return response.data(res, file)
    //     } catch (err) {
    //         return response.failure(res, err.message)
    //     }
    // }
    
    // exports.get = async (req, res) => {
    //     try {
    //         const file = await service.get(req)
    //         return response.data(res, file)
    //     } catch (err) {
    //         return response.failure(res, err.message)
    //     }
    // }
    
    // exports.update = async (req, res) => {
//     try {
//         const file = await service.update(req.params.id, req.body, req.context)
//         return response.data(res, file)
//     } catch (err) {
//         return response.failure(res, err.message)
//     }
// }