'use strict'

const fs = require('fs')
const specs = require('../specs')
const api = require('../api')
var auth = require('../permit')
const validator = require('../validators')
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');

var multipart = require('connect-multiparty')
var multipartMiddleware = multipart()


const configure = (app, logger) => {
    const log = logger.start('settings:routes:configure')

    app.get('/specs', function (req, res) {
        fs.readFile('./public/specs.html', function (err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                })
            }
            res.contentType('text/html')
            res.send(data)
        })
    })

    app.get('/api/specs', function (req, res) {
        res.contentType('application/json')
        res.send(specs.get())
    })


     // .......................users routes..............................
     app.get('/api/users', auth.context.builder,  api.users.get);
     app.post('/api/users/verifyRegOtp', auth.context.builder,  api.users.verifyRegOtp);
     app.post('/api/users', auth.context.builder, validator.users.canCreate, api.users.create);
 
     app.get('/api/users/:id', auth.context.builder,  validator.users.getById, api.users.getById);
     app.put('/api/users/:id', auth.context.builder, validator.users.update, api.users.update);
     app.post('/api/users/login', auth.context.builder, validator.users.login, api.users.login);
 
     app.post('/api/users/forgotPassword', auth.context.builder, validator.users.forgotPassword, api.users.forgotPassword);
     app.post('/api/users/verifyOtp', auth.context.builder,  validator.users.verifyOtp, api.users.verifyOtp);
 
     app.put('/api/users/changePassword/:id', auth.context.builder, validator.users.changePassword, api.users.changePassword);
     app.post('/api/users/logOut/:id', auth.context.builder, api.users.logOut)
 

    //.................... category routes.................................
    app.post('/api/categories', auth.context.builder, multipartMiddleware, api.categories.create);
    app.get('/api/categories/:id', multipartMiddleware, validator.categories.getById, api.categories.getById);
    app.get('/api/categories', multipartMiddleware, api.categories.get);
    app.put('/api/categories/update/:id', multipartMiddleware, validator.categories.update, api.categories.update);


    //...................similarProducts routes................................
    app.post('/api/similarProducts', multipartMiddleware, api.similarProducts.create);
    app.get('/api/similarProducts', multipartMiddleware, api.similarProducts.getByCategory);
    app.get('/api/similarProducts/:id', multipartMiddleware, api.similarProducts.getById);
    app.put('/api/similarProducts/update/:id', multipartMiddleware, validator.similarProducts.update, api.similarProducts.update)


    //........................ products routes....................................
    app.post('/api/products', multipartMiddleware, api.products.create);
    app.get('/api/products/:id', multipartMiddleware, validator.products.getById, api.products.getById);
    app.get('/api/products', multipartMiddleware, api.products.get);
    app.put('/api/products/update/:id', multipartMiddleware, validator.products.update, api.products.update);


    //................................. files/images routes..........................
    // app.post('/api/files', multipartMiddleware, api.files.create);
    // app.get('/api/files/:id', multipartMiddleware, validator.files.getById, api.files.getById);
    // app.get('/api/files', multipartMiddleware, api.files.get);
    // app.put('/api/files/update/:id', multipartMiddleware, api.files.update);
    // upload file route
    app.post('/api/files/upload', multipartMiddleware, api.files.upload);

    log.end()
}

exports.configure = configure

















// //.............................. feedbacks routes...............................
    // app.post('/api/feedbacks', api.feedbacks.create);
    // app.get('/api/feedbacks/:id', api.feedbacks.getById);
    // app.get('/api/feedbacks', api.feedbacks.get);


    // //.............................. aboutUS routes...............................
    // app.post('/api/aboutUs', api.aboutUs.create);
    // app.get('/api/aboutUs/:id', api.aboutUs.getById);
    // app.get('/api/aboutUS', api.aboutUs.get);
    // app.put('/api/aboutUs/update/:id', api.aboutUs.update);



    // //......................... cart routes..................................
    // app.post('/api/carts', api.carts.create);
    // app.get('/api/carts/:id', auth.context.builder, api.carts.get);
    // app.put('/api/carts/update/:id', auth.context.builder, api.carts.update);


    // //..............................order routes......................................
    // app.post('/api/orders', api.orders.create);
    // app.get('/api/orders/:id', validator.orders.getById, api.orders.getById);
    // app.get('/api/orders', api.orders.get);
    // app.put('/api/orders/update/:id', validator.orders.update, api.orders.update);