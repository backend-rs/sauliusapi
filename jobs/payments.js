'use strict'
const cron = require('cron').CronJob

const start = (startOn) => {
    let job = new cron({
        cronTime: startOn,
        onTick: () => {
            console.log('running a task at 5 am daily.');
        },
        start: true
    })
}
console.log('After job instantiation');
exports.schedule = () => {
    start(`0 5 * * *`)
}
