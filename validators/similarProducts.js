'use strict'

const response = require('../exchange/response')

exports.canCreate = (req, res, next) => {
    if (!req.body.name) {
        response.failure(res, 'ProductName is required')
    }
    if (!req.body.brand) {
        response.failure(res, 'Product brand is required')
    }
    if (!req.body.heading && !req.body.heading.title) {
        response.failure(res, 'Product title is required')
    }
    if (!req.body.heading && !req.body.heading.description) {
        response.failure(res, 'Product description is required')
    }
    if (!req.body.dimensions && !req.body.dimensions.value) {
        response.failure(res, 'Product weight-value is required')
    }
    if (!req.body.dimensions && !req.body.dimensions.unit) {
        response.failure(res, 'Product dimension-unit is required')
    }
    if (!req.body.price && !req.body.price.value) {
        response.failure(res, 'Product price is required')
    }
    if (!req.body.price && !req.body.price.currency) {
        response.failure(res, 'Product currency is required')
    }
    return next()
}

exports.update = (req, res, next) => {
    if (!req.params && !req.params.id) {
        response.failure(res, 'id is required')
    }
    return next()
}


