'use strict'

// User Module
module.exports = {

    type: {
        type: String,
        default: 'user',
        enum: ['user', 'admin']
    },
    name: {
        type: String,
        lowercase: true,
        trim: true
    },
    email: {
        type: String,
        lowercase: true,
        trim: true
    },
    phone: {
        type: String,
        unique: true,
    },
    password: {
        type: String,
        limit: 10
    },
    tempToken: String,
    regToken: String,
    expiryTime: String,
    otp: String,
    isVerified: {
        type: Boolean,
        default: false
    },
    token: String,
    address: {
        line1: String,
        line2: String,
        city: String,
        district: String,
        state: String,
        pinCode: String,
        country: String,
        location: {
            longitude: String,
            latitude: String
        }
    }
    // token: String,

}