'use strict'

// Product Module
module.exports = {

    category: {
        _id: String
    },
    isSimilarProduct: {
        type: Boolean,
        default: true,
    },
    name: String,
    brand: String,
    image: {
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String,
    },
    heading: {
        title: String,
        description: String
    },
    dimensions: [{
        value: String,
        unit: String
    }],
    price: {
        value: Number,
        currency: String
    },
    status: {
        type: String,
        default: 'enabled',
        enum: ['enabled', 'disabled']
    },
    isAdded: {
        type: String,
        default: 'false',
        enum: ['true', 'false']
    },
    shouldImageUpdate: {
        type: String,
        default: 'false',
        enum: ['true', 'false']
    },
    isUpdate: {
        type: String,
        default: 'false',
        enum: ['true', 'false']
    }

}