'use strict'

module.exports = {
        name: String,
        image: {
            url: String,
            thumbnail: String,
            resize_url: String,
            resize_thumbnail: String,
        },
        status: {
            type: String,
            default: 'active',
            enum: ['active', 'inactive']
        },
        shouldImageUpdate:{
            type: String,
            default: 'false',
            enum: ['true', 'false']
        },
        isUpdate:{
            type: String,
            default: 'false',
            enum: ['true', 'false'] 
        },
        count:String,
        skipCount:String,
        totalCount:String,
        products: [{
            _id:String,
            name: String,
            brand: String,
            image: {
                url: String,
                thumbnail: String,
                resize_url: String,
                resize_thumbnail: String,
            },
            heading: {
                title: String,
                description: String
            },
            dimensions: [{
                value: String,
                unit: String
            }],
            price: {
                value: String,
                currency: String
            },
            status: {
                type: String,
                default: 'enabled',
                enum: ['enabled', 'disabled']
            },
            isAdded: {
                type: String,
                default: 'false',
                enum: ['true', 'false']
            },
            shouldImageUpdate:{
                type: String,
                default: 'false',
                enum: ['true', 'false']
            },
            isUpdate:{
                type: String,
                default: 'false',
                enum: ['true', 'false'] 
            }
        }]

    
}