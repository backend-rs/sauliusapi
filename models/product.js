'use strict'

// Product Module
module.exports = {
    name: String,
    brand: String,
    image: {
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String,
    },
    heading: {
        title: String,
        description: String
    },
    dimensions: [{
        value: String,
        unit: String
    }],
    price: {
        value: Number,
        currency: String
    },
    status: {
        type: String,
        default: 'enabled',
        enum: ['enabled', 'disabled']
    },
    isAdded: {
        type: String,
        default: 'false',
        enum: ['true', 'false']
    },
    shouldImageUpdate: {
        type: String,
        default: 'false',
        enum: ['true', 'false']
    },
    isUpdate: {
        type: String,
        default: 'false',
        enum: ['true', 'false']
    }

}





































// module.exports = {
//         productName: {
//             type: String,
//             required: [true, 'Enter  ProductName']
//         },
//         brand: {
//             type: String,
//             required: [true, 'Enter Brand']
//         },
//         weight: {
//             type: String,
//             required: [true, 'Enter  Weight']
//         },
//         weightDimension: {
//             type: String,
//             required: [true, 'Enter weightDimension']
//         },
//         price: {
//             type: String,
//             required: [true, 'Enter  Price']
//         },
//         detailHeading: {
//             type: String,
//             required: [true, 'Enter product  heading']
//         },
//         detailDescription: {
//             type: String,
//             required: [true, 'Enter product detail']

//         },
//     similarProduct: {
//         categoryName: String,
//         products: [{
//             productName: {
//                 type: String,
//                 required: [true, 'Enter  ProductName']
//             },
//             brand: {
//                 type: String,
//                 required: [true, 'Enter Brand']
//             },
//             weight: {
//                 type: String,
//                 required: [true, 'Enter  Weight']
//             },
//             weightDimension: {
//                 type: String,
//                 required: [true, 'Enter weightDimension']
//             },
//             price: {
//                 type: String,
//                 required: [true, 'Enter  Price']
//             },
//             detailHeading: {
//                 type: String,
//                 required: [true, 'Enter product  heading']
//             },
//             detailDescription: {
//                 type: String,
//                 required: [true, 'Enter product detail']
//             }
//         }]
//     }
// }